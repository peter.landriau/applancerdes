package com.btsinfo.application_des;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.security.SecureRandom;

public class ActivityResult extends AppCompatActivity {

    TextView tvResultF;
    TextView tvDe1,tvDe2,tvDe3,tvDe4,tvDe5;
    private Button btReload, btChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        tvResultF = (TextView) findViewById(R.id.tvResultF);
        tvDe1 = (TextView) findViewById(R.id.tvDe1);
        tvDe2 = (TextView) findViewById(R.id.tvDe2);
        tvDe3 = (TextView) findViewById(R.id.tvDe3);
        tvDe4 = (TextView) findViewById(R.id.tvDe4);
        tvDe5 = (TextView) findViewById(R.id.tvDe5);

        Intent rIntent = getIntent();
        String monResult = "Vous avez sélectionné " + rIntent.getStringExtra("Faces") + " faces.";
        tvResultF.setText(monResult);


        SecureRandom random = new SecureRandom();

        String nbfaces = rIntent.getStringExtra("Faces");
        int nbfaces2 = Integer.parseInt(nbfaces);
        Integer de1 = random.nextInt(nbfaces2) + 1;
        Integer de2 = random.nextInt(nbfaces2) + 1;
        Integer de3 = random.nextInt(nbfaces2) + 1;
        Integer de4 = random.nextInt(nbfaces2) + 1;
        Integer de5 = random.nextInt(nbfaces2) + 1;

        String nbdes = rIntent.getStringExtra("Des");
        int nbdes2 = Integer.parseInt(nbdes);

        tvDe3.setText (de3.toString());
        if (nbdes2 > 1 )
        {
            tvDe2.setVisibility(View.VISIBLE);
            tvDe2.setText(de2.toString());
            if (nbdes2 > 2 )
            {
                tvDe4.setVisibility(View.VISIBLE);
                tvDe4.setText(de4.toString());
                if (nbdes2 > 3 )
                {
                    tvDe1.setVisibility(View.VISIBLE);
                    tvDe1.setText(de1.toString());
                    if (nbdes2 > 4 )
                    {
                        tvDe5.setVisibility(View.VISIBLE);
                        tvDe5.setText(de5.toString());
                    }
                }
            }
        }





        btChange = (Button) findViewById(R.id.btChange);
        btChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toMain();
            }
        });

        btReload = (Button) findViewById(R.id.btReload);
        btReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLancer();
            }
        });


    }

    public void toMain()
    {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    public void doLancer()
    {
        finish();
        startActivity(getIntent());
    }
}
