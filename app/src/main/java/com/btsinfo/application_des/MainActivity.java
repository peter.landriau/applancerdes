package com.btsinfo.application_des;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private Spinner spFaces, spDes;
    private String choixFaces, choixDes;
    private Button btLancer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spFaces = (Spinner) findViewById(R.id.spFaces);
        spDes = (Spinner) findViewById(R.id.spDes);


        ArrayAdapter<CharSequence> adapterF = ArrayAdapter.createFromResource(this,R.array.faces,android.R.layout.simple_spinner_item);
        adapterF.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFaces.setAdapter(adapterF);

        spFaces.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                choixFaces = adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<CharSequence> adapterD = ArrayAdapter.createFromResource(this,R.array.des,android.R.layout.simple_spinner_item);
        adapterD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDes.setAdapter(adapterD);

        spDes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                choixDes = adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btLancer = (Button) findViewById(R.id.btLancer);
        btLancer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toResult();
            }
        });

    }


    public void toResult()
    {
        Intent intent = new Intent(this,ActivityResult.class);
        intent.putExtra("Faces", choixFaces);
        intent.putExtra("Des", choixDes);
        startActivity(intent);
    }
}
